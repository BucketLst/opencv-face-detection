﻿
using Org.FaceDetectionUsingOpenCV.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Org.FaceDetectionUsingOpenCV
{
   public class IntelImage : IDisposable
    {
        IntPtr iplImagePointer;
        IplImage iplImageStruct;
        public int Stride { get { return iplImageStruct.widthStep; } }
        public IntPtr IplImage()
        {
            return iplImagePointer;
        }

        public IntelImage(int width, int height, bool color = true)
        {
            iplImagePointer = NativeMethods.cvCreateImage(new CvSize() { width = width, height = height }, 8, color ? 3 : 1);
            iplImageStruct = (IplImage)Marshal.PtrToStructure(iplImagePointer, typeof(IplImage));
        }

        public void CopyPixels(byte[] sourcePixelBuffer, int startIndex = 0)
        {
            Marshal.Copy(sourcePixelBuffer, startIndex, iplImageStruct.imageData, sourcePixelBuffer.Length);
        }
       
        public void Dispose()
        {
            NativeMethods.cvReleaseImage(ref iplImagePointer);
            GC.SuppressFinalize(this);
            GC.Collect();
        }

        ~IntelImage()
        {
            Dispose();
        }
    }
}
