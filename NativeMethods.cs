﻿
using Org.FaceDetectionUsingOpenCV.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Org.FaceDetectionUsingOpenCV
{
   public class NativeMethods
    {
       
       [DllImport(DllPaths.KERNEL32_DLL_PATH, SetLastError = true)]
        public static extern IntPtr LoadLibrary(string dllToLoad);

      
       [DllImport(DllPaths.KERNEL32_DLL_PATH, SetLastError = true)]
        public static extern bool FreeLibrary(IntPtr hModule);

      
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvCreateImage", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvCreateImage(CvSize size, int depth, int channels);

    
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvReleaseImage", CallingConvention = CallingConvention.Cdecl)]
        public static extern void cvReleaseImage(ref IntPtr image);

      
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvLoad", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvLoad([In][MarshalAs(UnmanagedType.LPStr)]string filename, IntPtr storage, IntPtr name,
            IntPtr real_name);

     
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvCreateMemStorage", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvCreateMemStorage(int block_size = 0);

       
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvClearMemStorage", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvClearMemStorage(IntPtr memory_storage);

       
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvReleaseMemStorage", CallingConvention = CallingConvention.Cdecl)]
        public static extern void cvReleaseMemStorage(ref IntPtr storage);

        
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvGetSeqElem", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvGetSeqElem(IntPtr sequence, int index);

      
       [DllImport(DllPaths.OPENCV_CORE_DLL_PATH, EntryPoint = "cvSeqPopFront", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvSeqPopFront(IntPtr sequence, IntPtr dest);

      
       [DllImport(DllPaths.OPENCV_OBJDETECT_DLL_PATH, EntryPoint = "cvReleaseHaarClassifierCascade", CallingConvention =
            CallingConvention.Cdecl)]
        public static extern void cvReleaseHaarClassifierCascade(ref IntPtr cascade);

      
       [DllImport(DllPaths.OPENCV_OBJDETECT_DLL_PATH, EntryPoint = "cvHaarDetectObjects", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr cvHaarDetectObjects(IntPtr image, IntPtr cascade, IntPtr mem_storage,
            double scale_factor, int min_neighbours, int flags,
            CvSize min_size, CvSize max_size);

       [DllImport("gdi32.dll")]
       [return: MarshalAs(UnmanagedType.Bool)]
       public static extern bool DeleteObject(IntPtr hObject);
    }
}
