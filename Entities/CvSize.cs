﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Org.FaceDetectionUsingOpenCV.Entities
{
    [StructLayout(LayoutKind.Sequential)]
    public struct CvSize
    {
        public int width;
        public int height;

        public CvSize(int width, int height) { this.width = width; this.height = height; }
    }
}
