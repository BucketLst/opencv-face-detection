﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Org.FaceDetectionUsingOpenCV.Entities
{
    [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
    public struct IplImage
    {
        public int nSize;
        public int ID;
        public int nChannels;
        public int alphaChannel;
        public int depth;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
        public string colorModel;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 4)]
        public string channelSeq;
        public int dataOrder;
        public int origin;
        public int align;
        public int width;
        public int height;
        public System.IntPtr roi;
        public System.IntPtr maskROI;
        public System.IntPtr imageId;
        public System.IntPtr tileInfo;
        public int imageSize;
        public System.IntPtr imageData;
        public int widthStep;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.I4)]
        public int[] BorderMode;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4, ArraySubType = UnmanagedType.I4)]
        public int[] BorderConst;
        public System.IntPtr imageDataOrigin;
    }
}
