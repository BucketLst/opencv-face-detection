﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Org.FaceDetectionUsingOpenCV
{
   public class DllPaths
    {
        public const string KERNEL32_DLL_PATH = "kernel32.dll";

        public const string OPENCV_CORE_DLL_PATH = @".\Assets\Dll\opencv_core230.dll";

        public const string OPENCV_TBB_DLL_PATH = @".\Assets\Dll\tbb.dll";

        public const string OPENCV_IMGPROC_DLL_PATH = @".\Assets\Dll\opencv_imgproc230.dll";

        public const string OPENCV_OBJDETECT_DLL_PATH = @".\Assets\Dll\opencv_objdetect230.dll";

        public const string OPENCV_HIGHGUI_DLL_PATH = @".\Assets\Dll\opencv_highgui230.dll";

        public const string OPENCV_FEATURES_DLL_PATH = @".\Assets\Dll\opencv_features2d230.dll";

        public const string OPENCV_FLANN_DLL_PATH = @".\Assets\Dll\opencv_flann230.dll";

        public const string OPENCV_CALIB_DLL_PATH = @".\Assets\Dll\opencv_calib3d230.dll";
       
    }
}
