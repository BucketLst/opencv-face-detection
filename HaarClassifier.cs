﻿
using Org.FaceDetectionUsingOpenCV.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Org.FaceDetectionUsingOpenCV
{
   public class HaarClassifier : IDisposable
    {
       List<IntPtr> pntr = null;
        IntPtr dllHandle, haarCascade, memoryStorage;

       public void LoadRequiredDll() 
        {
            try
            {
                List<string> dll = new List<string>();
                dll.Add(DllPaths.OPENCV_TBB_DLL_PATH);
                dll.Add(DllPaths.OPENCV_IMGPROC_DLL_PATH);
                dll.Add(DllPaths.OPENCV_HIGHGUI_DLL_PATH);
                dll.Add(DllPaths.OPENCV_FLANN_DLL_PATH);
                dll.Add(DllPaths.OPENCV_FEATURES_DLL_PATH);
                dll.Add(DllPaths.OPENCV_CALIB_DLL_PATH);
                dll.Add(DllPaths.OPENCV_OBJDETECT_DLL_PATH);

                foreach (var path in dll)
                {
                    LoadObjDetectDll(path);
                }
            }
            catch(Exception)
            {
                throw new Exception();
            }
        }
        
       public HaarClassifier(string cascadeFilePath) 
        {
            try
            {
                pntr = new List<IntPtr>();

                LoadRequiredDll();

                haarCascade = NativeMethods.cvLoad(cascadeFilePath, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);

                memoryStorage = NativeMethods.cvCreateMemStorage();
            }
            catch(Exception)
            {
                throw new Exception("Unable to Load HaarClassifier");
            }
        }
               
       void LoadObjDetectDll(string dll)
        {
            dllHandle = NativeMethods.LoadLibrary(dll);

            pntr.Add(dllHandle);
           
            if (dllHandle == IntPtr.Zero) 
                throw new System.ComponentModel.Win32Exception(Marshal.GetLastWin32Error());
        }
               
       [Flags]
       public enum DetectionFlags : int
        {
            None = 0,
            DoCannyPruning = 1,
            ScaleImage = 2,
            FindBiggestObject = 4,
            DoRoughSearch = 8
        }
               
       public LinkedList<CvRect> DetectObjects(IntelImage sourceImage, double scaleFactor = 1.1, int minNeighbours = 3,
            DetectionFlags flags = DetectionFlags.FindBiggestObject, CvSize minSize = default(CvSize), CvSize maxSize = default(CvSize))
        {
            try
            {
                NativeMethods.cvClearMemStorage(memoryStorage);

                LinkedList<CvRect> result = new LinkedList<CvRect>();

                IntPtr faceSequence = NativeMethods.cvHaarDetectObjects(sourceImage.IplImage(), haarCascade, memoryStorage,
                scaleFactor, minNeighbours, (int)flags, minSize, maxSize);

                for (; ; )
                {
                    IntPtr faceRectPointer = NativeMethods.cvGetSeqElem(faceSequence, 0);

                    if (faceRectPointer == IntPtr.Zero) break;
                    NativeMethods.cvSeqPopFront(faceSequence, IntPtr.Zero);

                    result.AddFirst((CvRect)Marshal.PtrToStructure(faceRectPointer, typeof(CvRect)));
                }
                return result;
            }
            catch
            {
                throw new Exception("Failed To Detect  Objects");
            }
           
        }

       public void UnloadLibrary()
        {
            foreach (var ptr in pntr)
            {
                NativeMethods.FreeLibrary(ptr);
            }
        }
              
       public void Dispose()
        {
            NativeMethods.cvReleaseMemStorage(ref memoryStorage);
            NativeMethods.cvReleaseHaarClassifierCascade(ref haarCascade);
            UnloadLibrary();
            pntr = null;     
            GC.SuppressFinalize(this);
            GC.Collect();
        }

       ~HaarClassifier()
        {
            Dispose();
        }
    }
}
